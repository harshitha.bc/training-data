// server.js File

import fetch from "node-fetch";
import _ from "lodash";
import fs from "fs";
import csvwriter from "csv-writer";
let createCsvWriter = csvwriter.createObjectCsvWriter;

import express from "express";
const app = express();

let accessToken, agent_id, base_url, startEpoch, endEpoch;
// app.get('/', async function(req, respond, next){
//   const startDate = req.query.start_time;
//   console.log("\n\n startDate",startDate);

//   console.log("\n\n startEpoch",startEpoch);

//   // let data = await firstcall();
// })



const csvWriter = createCsvWriter({
  path: "public/data2.csv",
  header: [
    // Title of the columns (column_names)
    { id: "skillName", title: "skill" },
    // { id: "questionName", title: "questions" },
    { id: "trainingData", title: "training" },
  ],
});
// app.listen(3000, () => console.log("listening on port 3000"));

let user_Arr = [],
  finalArr = [];
  let totalentries, dataobtained,finaldataobtained,url;

  const processJSON = (dataobtained) => {
    let questions = [];
    let totalData = dataobtained.intents
      ? dataobtained.intents.filter((item) => {

        return (item?.name.toLowerCase() == item?.skill?.name.toLowerCase() || item?.skill?.type == "knowledge-v2")
    })
      : null;

      let training = []

      let intentNames = totalData.map((intent) => {
        let intentName = intent.name;
        intent.training_data.map((td) =>{
          training.push(td?.value)
        })
        user_Arr.push({
          skillName: intentName,
          trainingNames: training,
        });
        training = []
      });
    
    return user_Arr;
  };

  let checkArr = [], checkData = [], uniqueChars = [];;
  const finalPush = async function (user_Arr) {
    let questionName, checkName
    
    user_Arr.forEach((val,index) =>{
      let skill = val.skillName
      val.trainingNames.forEach((td) => {
        let trainingData = td;
        finalArr.push({skillName:skill, trainingData});

      })    
      // finalArr.push({skillName:skill, trainingData});
    })
    return finalArr;
  };

const filterData = async (url, accessToken) => {
  const myHeaders = {
    "Access-Token": `${accessToken}`,
    "Content-Type": "application/json",
  };
  console.log(myHeaders);
  let requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  await fetch(
    url, requestOptions
  )
    .then((response) => response.text())
    .then((finaldataobtained1) => {
       finaldataobtained = JSON.parse(finaldataobtained1);
       console.log("\n\n Entries",finaldataobtained.total_entries)
    });
    let result = await fetch(`${url}?per_page=${finaldataobtained.total_entries}`,requestOptions)
      .then((response) => response.text())
      .then((dataobtained1) =>{
        dataobtained = JSON.parse(dataobtained1)
       processJSON(dataobtained);
      // console.log("\n\nuser_Arr",user_Arr)
      finalPush(user_Arr);
      // console.log("\n\n TD",user_Arr)
      return finalArr;
    });
  return result;
}

async function firstcall(instance,botid,accessToken) {
  url = `${instance}/api/v1/agents/${botid}/intents.json`
  let data = await filterData(url,accessToken);
  console.log(data);
  return await csvWriter
    .writeRecords(data)
    .then(() => {
      console.log("Data uploaded into csv successfully");
    })
    .catch((err) => {
      console.log(err);
      return null;
    });
}

// pass input(instance,bot Id, access key)
firstcall(`https://c10.avaamo.com`,2569,`6f56910e0b544032826e23e1ca521720`)


// app.listen(8000, () => console.log("listening on port 8000"));
