//input params
//botid - this you need to pass to the api - 24419
//access_token - this is passed in the api header Access-Token - 9a8bf31bf18e443485f2ed224db4c979
//instance - https://c0.avaamo.com

// const express = require('express');
// var cors = require('cors');
// const app = express();

// app.use(cors());
// app.use(express.json());

// https://c10.avaamo.com/api/v1/agents/2569/intents.json

const express = require('express');
var cors = require('cors')
const app = express();
app.use(express.json());
app.use(cors())

let instance="https://c10.avaamo.com"
let botId = 2569
let accessToken = "6f56910e0b544032826e23e1ca521720"

let url = instance+"/api/v1/agents/"+botId+"/"+accessToken

let inputParams = {        
  botId : botId,
  accessToken: accessToken,
  instance: instance
}
var postProd = {
  method: 'GET',
  headers: {
     'Content-Type': 'application/json'
  },
  body: JSON.stringify(inputParams)
}
fetch('url',postProd)

//output would be a csv file

//get the count of total intents in the bot per_page=1
//https://docs.avaamo.com/user-guide/ref/avaamo-platform-api-documentation/agent-api/intents

//use the total_entries count from the previous response and call the same api with param per_page = total_entries to get the total intents.

//fetches only top level intents - dialog and QnA
let dialogIntents = intents.filter((intent) => {
    return (
      intent?.name.toLowerCase() == intent?.skill?.name.toLowerCase() ||
      intent?.skill?.type == "knowledge-v2"
    );
  });
  
  //code to fetch all the training data corresponding to the intent name.
  //{"intent_name_1":["training_data_1","training_data_2",...],"intent_name_2":["training_data_1","training_data_2",...],...}
  let intentNames = dialogIntents.map((intent) => {
    //
    return intent.name;
  });
  
  //convert the data to csv and output a csv file - use AWS Lambda for this.
  
  //console.log(dialogIntents[0]);
  app.listen(3000, () => console.log("listening on port 3000"));
